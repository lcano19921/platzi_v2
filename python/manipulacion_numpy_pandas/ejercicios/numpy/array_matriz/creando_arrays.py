'''Creando arrays
'''

import numpy as np

#podemos crear listas o arrays directamente desde np
#array desde cero hasta 9, ya que son 10 posiciones pero inicia en cero
arr = np.arange(0,10)
print(arr)

#tambien se pude incremental, en este caso de dos
arr = np.arange(0,10,2)
print(arr)

#un array de ceros (con 3 ceros)
arr = np.zeros(3)
print(arr)

#tambien tipo matriz
mat = np.zeros((10,10))
print(mat)

#crear distribuciones d enuemros, como una especie de aletarorio
arr = np.linspace(0,10,100)
print(arr)