'''Dimensiones
'''

import numpy as np

# 0 dimension - unico valor: scalar . escalar
# 1 dimension: Vector
# 2 dimensiones: Matrix
# 3 dimensiones en adelante: Tensor 

#probamos esclara de cero simensiones
scalar = np.array(42)
print(scalar)
#validamos la cantidad de dimensiones
print(scalar.ndim) # 0

#probamos Vector de una simension
vector = np.array([1,2])
print(vector)
#validamos la cantidad de dimensiones
print(vector.ndim) # 1

#probamos matroz de dos dimensiones
matirz = np.array([[1,2],[2,3]])
print(matirz)
#validamos la cantidad de dimensiones
print(matirz.ndim) # 2

# los tensores son algo mucho más complejo
# son matrices dentro de matrices
tensor = np.array([[[1,2],[2,3]],[[1,2],[2,3]]])
print(tensor)
#validamos la cantidad de dimensiones
print(tensor.ndim) # 3

############# AGREGAR O ELIMINAR DIMENSIONES ##############
# usando ndim podemos crear dimesiones como queramos, y se crean en blanco por defecto
vector = np.array([1,2,3], ndmin=10)
print(vector)
print(vector.ndim)

# expandir unicamente en sus ejes, es decir filas o colunas
# las filas con axis : 0 = filas   1 = columnas

expand = np.expand_dims(np.array([1,2,3]), axis=0)
print(expand)
print(expand.ndim)

# eliminar dimensiones que no se usan
# usando squeeze el me lo crea de forma correcta
tensor = np.array([1,2,3], ndmin=10) # tensor con 10 dimensiones, pero solo una valida
print(tensor) # vemos el tensor
print(tensor.ndim) # tiene 10 dimensiones 

tensor2 = np.squeeze(tensor) # en esta linea lo reducimos
print(tensor2) # vemos el tensor
print(tensor2.ndim) # tiene 10 dimensiones 