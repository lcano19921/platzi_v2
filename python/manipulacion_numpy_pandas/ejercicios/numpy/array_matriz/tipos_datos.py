'''Tipos de Datos
'''

import numpy as np


#se puede crear un array con un rango especifico
arr = np.arange(0,10)
# print(arr)

# podemos validar los tipos de datos que contiene el array
arr = np.array([1,2,3,4])
print(arr.dtype)

#creamos un array con tipos de datos especificos
arr = np.array([1,2,3,4], dtype=float)
print(arr)
print(arr.dtype)

#cambiamos el tipo de dato del array
arr = arr.astype(np.int)
print(arr.dtype)

#tambien se puden cambiar a booleano
#tamien de string a entero, pero desde que sean "numeros"

# solo puede contener un mismo tipo de dato en total

