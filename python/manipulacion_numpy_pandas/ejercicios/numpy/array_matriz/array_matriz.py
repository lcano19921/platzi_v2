import pandas as pd
import numpy as np

#arrays en numpy, es el principal objeto y nace todo aqui

#creamos una lista normal de python
lista = [1, 2 , 3, 4, 5, 6, 7, 8, 9]

#creamos un array de np
arr = np.array(lista)
print(type(arr))
print(arr)
print(lista)

# lo mismo de lo anterior pero con matriz
matriz_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
matriz = np.array(matriz_1)
print(type(matriz))
print(matriz)
print(matriz_1)

#indexing
#los array empiezan en cero
print(arr[0])

#se pueden sumar
sum = arr[0] + arr[5]

#matriz
print(matriz[0])

#primer valor filas, segundo columnas
print(matriz[0,2])

#rango de elementos lista
#slising
print(arr[0:3])

#tambien podemos pedir los elementos del array [1, 2 , 3, 4, 5, 6, 7, 8, 9] en posiciones especificas
print(arr[::3]) # [1 4 7]

#valores de izquierda a derecha
print(arr[-3:]) #[7 8 9]

#matriz [[1, 2, 3], 
#       [4, 5, 6], 
#       [7, 8, 9]]
#podemos acceder a los elementos desde la fila 1 pero solo hasta la columna 2
print(matriz[1:,0:2]) #[[4 5]
                      # [7 8]



